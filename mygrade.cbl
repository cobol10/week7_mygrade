       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MYGRADE.
       AUTHOR. ANANCHAI.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "allgrade.txt"
             ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-GRADE-FILE ASSIGN TO "AVG.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  GRADE-FILE.
       
       01 GRADE-DETAILS.
          88 END-OF-GRADE-FILE                VALUE HIGH-VALUE.
       05 COURSE-CODE           PIC X(6).
          05 COURSE-NAME        PIC X(50).
          05 CREDIT             PIC 9(1).
          05 GRADE              PIC X(2).

       FD  AVG-GRADE-FILE.
       01 GRADE-AVG.
          05 AVG-GRADE          PIC 9.999.
          05 AVG-SCI-GRADE      PIC 9.999.
          05 AVG-CS-GRADE       PIC 9.999.

       WORKING-STORAGE SECTION.
       01 SUM-GRADE             PIC 9(3)V9.
       01 SUMAVG-GRADE          PIC 9V999.
       01 SUM-CREDIT            PIC 9(3).
       

       01 SUM-CREDIT-SCI        PIC 9(3)V999  VALUE ZERO.
       01 SUM-CREDIT-CS         PIC 9(3)V999  VALUE ZERO.

       01 SUM-AVG-SCI           PIC 9(1)V9(3).
       01 SUM-AVG-CS            PIC 9(1)V9(3).

       01 SCI-GRADE             PIC 9(3)V9.
       01 CS-GRADE              PIC 9(3)V9.


       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT GRADE-FILE
           OPEN OUTPUT AVG-GRADE-FILE
           PERFORM UNTIL END-OF-GRADE-FILE
                   DISPLAY COURSE-CODE
                           SPACE
                           COURSE-NAME
                           SPACE
                           CREDIT
                           SPACE
                           GRADE
                   READ GRADE-FILE 
                   AT END
                      SET END-OF-GRADE-FILE TO TRUE 
                   END-READ
                   IF NOT END-OF-GRADE-FILE THEN
                      PERFORM 001-PROCESS THRU 001-EXIT
                   END-IF
              
           END-PERFORM
           COMPUTE SUMAVG-GRADE = SUM-GRADE / SUM-CREDIT 
           COMPUTE SUM-AVG-SCI =SCI-GRADE /SUM-CREDIT-SCI  
           COMPUTE SUM-AVG-CS = CS-GRADE / SUM-CREDIT-CS
           MOVE SUMAVG-GRADE TO AVG-GRADE 
           MOVE SUM-AVG-SCI TO AVG-SCI-GRADE
           MOVE SUM-AVG-CS TO AVG-CS-GRADE
           WRITE GRADE-AVG
           CLOSE GRADE-FILE
           CLOSE AVG-GRADE-FILE
           GOBACK
           .
       001-PROCESS.
           EVALUATE TRUE
              WHEN GRADE = "A" COMPUTE SUM-GRADE = SUM-GRADE+(4*CREDIT)
              WHEN GRADE = "B+" COMPUTE SUM-GRADE=SUM-GRADE+(3.5*CREDIT)
              WHEN GRADE = "B" COMPUTE SUM-GRADE = SUM-GRADE +(3*CREDIT)
              WHEN GRADE = "C+" COMPUTE SUM-GRADE=SUM-GRADE+(2.5*CREDIT)
              WHEN GRADE = "C" COMPUTE SUM-GRADE = SUM-GRADE+(2*CREDIT)
              WHEN GRADE = "D+" COMPUTE SUM-GRADE=SUM-GRADE+(1.5*CREDIT)
              WHEN OTHER COMPUTE SUM-GRADE = SUM-GRADE + CREDIT
           END-EVALUATE
           COMPUTE SUM-CREDIT = SUM-CREDIT + CREDIT
           IF COURSE-CODE (1:1) IS EQUAL TO "3" THEN
           COMPUTE SUM-CREDIT-SCI = SUM-CREDIT-SCI + CREDIT
                 EVALUATE TRUE
                    WHEN GRADE = 
                    "A" COMPUTE SCI-GRADE = SCI-GRADE+(4*CREDIT)
                    WHEN GRADE = 
                    "B+"COMPUTE SCI-GRADE=SCI-GRADE+(3.5*CREDIT)
                    WHEN GRADE = 
                    "B" COMPUTE SCI-GRADE = SCI-GRADE+(3*CREDIT)
                    WHEN GRADE = 
                    "C+"COMPUTE SCI-GRADE=SCI-GRADE+(2.5*CREDIT)
                    WHEN GRADE = 
                    "C" COMPUTE SCI-GRADE = SCI-GRADE+(2*CREDIT)
                    WHEN GRADE = 
                    "D+"COMPUTE SCI-GRADE=SCI-GRADE+(1.5*CREDIT)
                    WHEN OTHER COMPUTE 
                    SCI-GRADE=SCI-GRADE+CREDIT
                 END-EVALUATE 
              IF COURSE-CODE (1:2) IS EQUAL TO "31" THEN 
                 COMPUTE SUM-CREDIT-CS=SUM-CREDIT-CS+CREDIT         
                 EVALUATE TRUE
                    WHEN GRADE = 
                    "A" COMPUTE CS-GRADE = CS-GRADE+(4*CREDIT)
                    WHEN GRADE = 
                    "B+"COMPUTE CS-GRADE=CS-GRADE+(3.5*CREDIT)
                    WHEN GRADE = 
                    "B" COMPUTE CS-GRADE = CS-GRADE+(3*CREDIT)
                    WHEN GRADE = 
                    "C+"COMPUTE CS-GRADE=CS-GRADE+(2.5*CREDIT)
                    WHEN GRADE = 
                    "C" COMPUTE CS-GRADE = CS-GRADE+(2*CREDIT)
                    WHEN GRADE = 
                    "D+"COMPUTE CS-GRADE=CS-GRADE+(1.5*CREDIT)
                    WHEN OTHER COMPUTE 
                    CS-GRADE=CS-GRADE+CREDIT
                 END-EVALUATE   
                 
              END-IF 
           END-IF        
           .
           
       001-EXIT.
           EXIT.